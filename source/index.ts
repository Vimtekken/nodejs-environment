import FS from 'fs';

/// Environment variable loader utility
class Environment {
	/// Provides utility for parsing boolean environment variables
	static stringToBool(someString: string): boolean {
		switch (someString.toLowerCase()) {
			case 'false':
			case 'no':
			case '0':
			case '':
				return false;
			default:
				return true;
		}
	}

	/// Loads the environment with the supplied name or at a file at FILE_envName. If neither returns default value.
	static getEnv<T>(envName: string, construct: (env: string) => T, defaultValue?: string): T {
		let value: string | undefined = process.env[envName];
		if (!value) {
			// Try to read file
			const fileName = process.env[`FILE_${envName}`];
			if (fileName) {
				if (FS.existsSync(fileName)) {
					value = FS.readFileSync(fileName, { encoding: 'utf-8' });
				}
			}
		}
		if (!value) {
			// Set default
			value = defaultValue;
		}
		return construct(value || '');
	}
}

export default Environment;
