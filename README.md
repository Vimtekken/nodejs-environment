# NodeJS Environment
Environment variable I like to reuse. Loads from environment or from a file with the name `FILE_${env var name}`

# License
This repo is licensed under the [Apache 2.0 License](https://apache.org/licenses/LICENSE-2.0).
