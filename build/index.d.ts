declare class Environment {
    static stringToBool(someString: string): boolean;
    static getEnv<T>(envName: string, construct: (env: string) => T, defaultValue?: string): T;
}
export default Environment;
//# sourceMappingURL=index.d.ts.map