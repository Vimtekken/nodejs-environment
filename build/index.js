"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
/// Environment variable loader utility
class Environment {
    /// Provides utility for parsing boolean environment variables
    static stringToBool(someString) {
        switch (someString.toLowerCase()) {
            case 'false':
            case 'no':
            case '0':
            case '':
                return false;
            default:
                return true;
        }
    }
    /// Loads the environment with the supplied name or at a file at FILE_envName. If neither returns default value.
    static getEnv(envName, construct, defaultValue) {
        let value = process.env[envName];
        if (!value) {
            // Try to read file
            const fileName = process.env[`FILE_${envName}`];
            if (fileName) {
                if (fs_1.default.existsSync(fileName)) {
                    value = fs_1.default.readFileSync(fileName, { encoding: 'utf-8' });
                }
            }
        }
        if (!value) {
            // Set default
            value = defaultValue;
        }
        return construct(value || '');
    }
}
exports.default = Environment;
//# sourceMappingURL=index.js.map